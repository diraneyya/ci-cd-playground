# GitLab CI/CD Variables

This page is built using embedded Ruby (i.e. eRuby) to create a markdown table, which is then converted to HTLML using the `nanoc` Static Site Generator (SSG).

This page was generated on _<%= Time.now.strftime("%A %d %B, %I:%M%p") %>_.

| Variable                                 |                    | Value              |
|------------------------------------------|--------------------|--------------------|<% 
  Hash[ENV.sort_by { |key, value| key.to_s }].each do | key, value | %>
| `<%= key %> `                            |&nbsp; &nbsp; &nbsp;| <%= (value.length > 64 ? value[0, 64] + "..." : value).dump %>       |<% 
  end %>
